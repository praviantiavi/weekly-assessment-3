const examData = [
  { id: 1, name: "Tony", score: 87 },
  { id: 2, name: "Steve", score: 91 },
  { id: 3, name: "Scott", score: 72 },
  { id: 4, name: "Natasha", score: 66 },
  { id: 5, name: "Bruce", score: 77 },
  { id: 6, name: "Denvers", score: 82 },
  { id: 7, name: "Pepper", score: 91 },
  { id: 8, name: "Clint", score: 84 },
  { id: 9, name: "Barton", score: 90 },
  { id: 10, name: "Stacey", score: 88 },
  { id: 11, name: "Wanda", score: 50 },
  { id: 12, name: "Peter", score: 79 },
  { id: 13, name: "James", score: 84 },
  { id: 14, name: "Shang", score: 85 },
];




// You can modify the variable name
//no.1
let averageScore = (data) => {
  let score = data.reduce((total, value) => total + value.score, 0);
  let dataScore = score / examData.length;
  return dataScore.toFixed(2);
}


//no.2
let highestStudents = (data) =>
  data.filter(
    (highStudent, index, array) =>
      array.reduce((total,value) => Math.max(total, value.score), 0) ===
      highStudent.score
  )
  .map((highStudent) => highStudent.name);




//no.3  
let lowestStudents = (data) =>
  data.filter(
    (student, index, array) =>
      array.reduce((total,value) => Math.min(total, value.score), 100) ===
      student.score
  )
  .map((student) => student.name);



//no.4
let failStudents = examData.filter((students) => students.score <= 75)
  .map ((students) => students.name);
  


//no.5
let passPercentage = (data) => {
  let passStudent = data.filter(students => students.score > 75);
  let percentStudent = passStudent.length / data.length *100;
  return `${percentStudent.toFixed(2)}%`;
}
 

console.log(`Average score: ${averageScore(examData)}`);
console.log(`Student who get highest score: ${highestStudents(examData)}`);
console.log(`Student who get lowest score: ${lowestStudents(examData)}`);
console.log(`Student who fail: ${failStudents}`);
console.log(`Pass percentage: ${passPercentage(examData)}`);






